#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`card` module 

:author: `FIL - Faculté des Sciences et Technologies - 
         Univ. Lille <http://portail.fil.univ-lille1.fr>`_

:date: 2017, september.
:last revision: 2024, march

"""
from __future__ import annotations
import random



class Card(object):
    """
    Cards are defined by a valeur and a couleur.
    Possible valeurs and couleurs are listed in ``Card.valeurS`` and ``Card.couleurS``.

    $$$ c1 = Card("Ace", "heart")
    $$$ c1.couleur
    'heart'
    $$$ c1.valeur
    'Ace'
    $$$ c1
    Card("Ace", "heart")
    $$$ c2 = Card("King", "spade")
    $$$ c2.valeur in Card.valeurS
    True
    $$$ c2.couleur in Card.couleurS
    True
    $$$ c1 == c1
    True
    $$$ c1 != c1
    False
    $$$ c1 < c1
    False
    $$$ c1 <= c1
    True
    """

    ## tuple of possible valeurs and couleurs in ascending order
    valeurS = ("Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Knight", "Queen", "King")
    couleurS = ("spade", "heart", "diamond", "club")

    def __init__(self, valeur: str, couleur: str):
        """
        creates a card with valeur and couleur

        précondition : valeur in valeurS and couleur in couleurS
        """
        self.valeur = valeur
        self.couleur = couleur


    def __hash__(self) -> int:
        """
        Renvoie un haché de self.
        """
        return hash((self.couleur,self.valeur))

    def __repr__(self) -> str:
        """
        return a string representation of the card
    
        $$$ repr(Card('Ace', 'heart'))
        'Card("Ace", "heart")'
        """
        return f'Card("{self.valeur}", "{self.couleur}")'

    def __str__(self) -> str:
        """
        return a string representation of the card
    
        $$$ str(Card('Ace', 'heart'))
        'Ace of heart'
        """
        return f'{self.valeur} of {self.couleur}'


    def compare(self, card: Card) -> int:
        """
        compares cards.

        Order on cards is defined  by order on valeurs

        return: 
          
           * a positive number if self is greater than card
           * a negative number if self is lower than card
           * 0 if self is the same than card

        précondition: none
        exemples: 

        $$$ c1 = Card('Ace', 'heart')
        $$$ c2 = Card('King', 'heart')
        $$$ c3 = Card('Ace','spade')
        $$$ c1bis = Card('Ace','heart')
        $$$ c1.compare(c2) < 0
        True
        $$$ c2.compare(c1) > 0
        True
        $$$ c1.compare(c3) == 0
        True
        """
        val1 = self.valeur
        val2 = card.valeur
        if Card.valeurS.index(val1) > Card.valeurS.index(val2):
            return 1
        elif Card.valeurS.index(val1) < Card.valeurS.index(val2):
            return -1
        else:
            return 0

    @staticmethod
    def deck(n_card: int) -> list[Card]:
        """
        return a list of `n_card` randomly chosen cards

        precondition: n_card > 0 and n_card <= 4*13

        Exemples:

        $$$ cartes = Card.deck( 10 )
        $$$ len(cartes) == 10
        True
        $$$ all( isinstance(c, Card) for c in cartes)
        True
        """
        res = []
        for i in range(n_card):
            val = Card.valeurS[random.randint(0,13)]
            cl = Card.couleurS[random.randint(0,3)]
            c = Card(val,cl)
            res.append(c)
        return res
    

    def __eq__(self, card: Card)->bool:
        """
        return True if self equals card
               False otherwise
        """
        return self.valeur == card.valeur

    def __neq__(self, card: Card) -> bool:
        """
        return True if self don't equal card
               False otherwise
        """
        return self.valeur != card.valeur

    def __lt__(self, card: Card) -> bool:
        """
        return True if self is strictly inferior to card
               False otherwise
        """
        return Card.compare(self, card) == -1

    def __le__(self, card: Card) -> bool:
        """
        return True if self is inferior or equal to card
               False otherwise
        """
        return Card.compare(self, card) == 1 or Card.compare(self, card) == 0

    def __gt__(self, card: Card) -> bool:
        """
        return True if self is strictly superior to card
               False otherwise
        """
        return Card.compare(self, card) == 1

    def __ge__(self, card: Card) -> bool:
        """
        return True if self is superior or equal to card
               False otherwise
        """
        return Card.compare(self, card) == 0


if (__name__ == '__main__'):
    import apl1test
    apl1test.testmod('card.py')

