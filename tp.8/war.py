#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`war` game

:author: `FIL - Faculté des Sciences et Technologies -
         Univ. Lille <http://portail.fil.univ-lille1.fr>`

:date: 2021, april.
:last revision: 2024, march.
"""

from card import Card
from apqueue import *
from apstack import *


def distribute(n_card: int) -> tuple[ApQueue, ApQueue]:
    """
    renvoie un couple (m1, m2) constitué de deux files,
    contenant pour chacune `n_card` cartes

    precondition : n_card > 0
    exemples :

    $$$ m1, m2 = distribute(4)
    $$$ len(m1) == 4
    True
    $$$ len(m2) == 4
    True
    $$$ type(m1) == ApQueue
    True
    $$$ type(m2) == ApQueue
    True
    """
    m1, m2 = ApQueue(), ApQueue()
    for i in range(n_card):
        m1.enqueue(Card.deck(n_card))
        m2.enqueue(Card.deck(n_card))
    return(m1,m2)
    
    

def gather_stack(main: ApQueue, pile: ApStack) -> None:
    """
    ajoute les carte de la pile dans la main

    exemples :

    $$$ cartes = Card.deck(4)
    $$$ main = ApQueue()
    $$$ pile = ApStack()
    $$$ for c in cartes: pile.push(c)
    $$$ gather_stack( main, pile )
    $$$ len( main ) == 4
    True
    $$$ all( main.dequeue() == cartes[ 3 - i ] for i in range(3))
    True
    """
    while not pile.is_empty():
        a = pile.pop()
        main.enqueue(a)

def play_one_round(m1: ApQueue, m2: ApQueue, pile: ApStack) -> None:
    """
    Simule une étape du jeu :
    `j1`` et ``j2`` prennent la première carte de leur
    main. On compare les deux cartes :

    * Si la carte de ``j1`` est supérieure à celle de ``j2``, alors
    ``j1`` remporte toutes les cartes de la pile ;
    * Si la carte de ``j1`` est inférieure à celle de ``j2``, alors
    ``j2`` remporte toutes les cartes de la pile ;
    * Si les cartes sont égales, alors elles sont *empilées* sur la
      pile.

    precondition : m1 et m2 ne sont pas vides
    """
    if len(pile) >= 0 :
        c1 , c2 = m1.dequeue(), m2.dequeue()
        if c1 < c2:
            while not pile.is_empty() :
                m2.enqueue(c)
            m2.enqueue(c1)
            m2.enqueue(c2)
        elif c1 > c2:
            while not pile.is_empty() :
                m1.enqueue(c)
            m1.enqueue(c1)
            m1.enqueue(c2)
            
        else:
            pile.push(c1)
            pil.push(c2)
        return f"joueur 1 joue {c1} et joueur 2 {c2}, len(pile) cartes sur la table"

        

def play(n_card: int, n_round: int) -> None:
    """
    simule une partie de bataille

    n_card: le nombre de cartes à distribuer à chaque joueur.
    n_round: le nombre maximal de tours
    """
    m1, m2 = distribute(n_card)
    pile = ApStack()
    while n_round > 0 or not m1.is_empty() or not m2.is_empty:
        play_one_round(m1,m2,pile)
        print(f"Le joueur 1 a {len(m1)} cartes et le joueur 2 a {len(m2)} cartes")
    if m1.is_empty():
        print(f"Le joueur 1 a 0 cartes et le joueur 2 a {len(m2)} cartes, Le joueur 2 a gagné")
    elif m2.is_empty():
        print(f"Le joueur 1 a {len(m1)} cartes et le joueur 2 a 0 cartes, Le joueur 1 a gagné")
    else:
        print(f"le nombre de tours a été atteint, il n'y a pas de gagnant")

play(4, 10)

if __name__ == "__main__":
    import apl1test
    apl1test.testmod("war.py")

