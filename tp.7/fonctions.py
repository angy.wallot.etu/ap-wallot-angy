# 20/03/24
# Angy Wallot
# tp Évaluation empirique des tris

import random
import timeit
import math

def liste_alea(n:int)->list:
    """
    qui construit une liste de longueur n contenant les entiers 0 à n-1 mélangés.
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    l = []
    for nb in range(n):
        l.append(nb)
    random.shuffle(l)
    return l

timeit.timeit(stmt='sqrt(2)')
timeit.timeit(stmt='sqrt(2)', setup='from math import sqrt')
timeit.timeit(stmt='sqrt(2)', setup='from math import sqrt', number=5000)
timeit.timeit(stmt='sqrt(2) ; sqrt(3)', setup='from math import sqrt')
