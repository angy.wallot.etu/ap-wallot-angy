# ANGY WALLOT
# GROUPE 15
# 31/01/2024 (absente)

import turtle

def flocon(l, n):
    if n == 0:
        turtle.forward(l)
    else:
        flocon(l / 3, n - 1)
        turtle.left(60)
        flocon(l / 3, n - 1)
        turtle.right(120)
        flocon(l / 3, n - 1)
        turtle.left(60)
        flocon(l / 3, n - 1)

#flocon(500,4)


def triangle(l, n):
    if n == 0:
        for _ in range(3):
            turtle.forward(l)
            turtle.left(120)
    else:
        l /= 2
        triangle(l, n - 1)
        turtle.forward(l)
        triangle(l, n - 1)
        turtle.backward(l)
        turtle.left(60)
        turtle.forward(l)
        turtle.right(60)
        triangle(l, n - 1)
        turtle.left(60)
        turtle.backward(l)
        turtle.right(60)
        
triangle(500, 1)