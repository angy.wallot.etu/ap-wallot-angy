from PIL import Image, ImageDraw
import csv
from decoupe_im import *
im = Image.open('images/calbuth.png')
im_rgb = im.convert('RGB')

class Bloc:
    def __init__(self, *args):
        """
        Creer un bloc contenant des arguments variables
        Exemple(s) :
        §§§ b1 = Bloc(((18,14),(0,0)), True, (255,255,255))
        §§§ b1
        Bloc('True' '((18,14),(0,0))','(255, 255, 255)')
        §§§ b2 = Bloc(((52, 86),(0,0)) False)
        §§§ b2
        Bloc('False' '((52, 86),(0,0))')

        """
        assert len(args) >= 2 and len(args) <= 3 #2 ou 3 arguments car la taille et l'uniforlité sont nécessaire
        assert type(args[0]) == tuple #la taille correspond à un tuple avec deux nombres dans chaque elements correspondant respectivement haut gauche et bas droit, le premier etant le pixel en haut à droite et en bas à droite
        self.taille = args[0]
        assert type(args[1]) == bool 
        self.uni = args[1]
        if len(args) > 2 and self.uni==True:
            assert type(args[2]) == tuple
            self.couleur = args[2]
        else:
            self.couleur = (0,0,0)
            

    def __repr__(self) -> str:
        """
        Renvoie la representation d'un bloc
        Exemples:
        §§§ b1 = Bloc((18,14),(0,0)), True, (255,255,255))
        §§§ b1
        Bloc('True' '((18,14),(0,0))','(255, 255, 255)')
        """
        p_args = f"'{self.uni}' '{self.taille}'"
        if self.couleur != (0,0,0):
            p_args += f",'{self.couleur}'"
        return f"Bloc({p_args})"
    
    def get_taille(self):
        """à_remplacer_par_ce_que_fait_la_fonction

        Précondition : 
        Exemple(s) :
        $$$ p = Bloc((56,85), False)
        $$$ p.get_taille()
        (56,85)

        """
        return self.taille
    
def decouper_image(im, ordre, hautgauche, basdroit, res):
    """
    Fonction qui stockent tout les blocs produits en decoupant l'image dans un fichier csv 

    Précondition : im doit etre convertit en rgb
    Exemple(s) :
#     $$$ decouper_image(im_rgb, 2, (0,0), (10,10), [])
#     [Bloc('True' '(2, 2)','(236, 210, 111)'), Bloc('True' '(5, 2)','(236, 210, 111)'), Bloc('True' '(2, 5)','(236, 210, 111)'), Bloc('True' '(5, 5)','(236, 210, 111)'), Bloc('True' '(5, 5)','(236, 210, 111)'), Bloc('True' '(5, 2)','(236, 210, 111)'), Bloc('True' '(10, 2)','(236, 210, 111)'), Bloc('True' '(5, 5)','(236, 210, 111)'), Bloc('True' '(10, 5)','(236, 210, 111)'), Bloc('True' '(10, 5)','(236, 210, 111)'), Bloc('True' '(2, 5)','(236, 210, 111)'), Bloc('True' '(5, 5)','(236, 210, 111)'), Bloc('True' '(2, 10)','(236, 210, 111)'), Bloc('True' '(5, 10)','(236, 210, 111)'), Bloc('True' '(5, 10)','(236, 210, 111)'), Bloc('True' '(5, 5)','(236, 210, 111)'), Bloc('True' '(10, 5)','(236, 210, 111)'), Bloc('True' '(5, 10)','(236, 210, 111)'), Bloc('True' '(10, 10)','(236, 210, 111)'), Bloc('True' '(10, 10)','(236, 210, 111)'), Bloc('True' '(10, 10)','(236, 210, 111)')]
#     $$$ decouper_image(im_rgb, 1, (0,0), (10,10), [])
#     [Bloc('True' '(5, 5)','(236, 210, 111)'), Bloc('True' '(10, 5)','(236, 210, 111)'), Bloc('True' '(5, 10)','(236, 210, 111)'), Bloc('True' '(10, 10)','(236, 210, 111)'), Bloc('True' '(10, 10)','(236, 210, 111)')]

    """
    if ordre != 0:
        b1_taille = ((0,0),(basdroit[0]//2, basdroit[1]//2))  # coordonnées correspondant respectivement à en haut à gaucge et en bas à droite
        b2_taille = ((basdroit[0]//2,0),(basdroit[0], basdroit[1]//2))
        b3_taille = ((0,basdroit[1]//2),(basdroit[0]//2,basdroit[1]))
        b4_taille = ((basdroit[0]//2,basdroit[1]//2),(basdroit[0],basdroit[1]))
        couleur_im = couleurs_moyenne(liste_couleur(im, (0,0), basdroit))
        couleur_b1 = couleurs_moyenne(liste_couleur(im, b1_taille[0], b1_taille[1]))
        couleur_b2 = couleurs_moyenne(liste_couleur(im, b2_taille[0], b2_taille[1]))
        couleur_b3 = couleurs_moyenne(liste_couleur(im, b3_taille[0], b3_taille[1]))
        couleur_b4 = couleurs_moyenne(liste_couleur(im, b4_taille[0], b4_taille[1]))
        bloc1 = Bloc(b1_taille,True, couleur_b1)  # coordonnées correspondant respectivement à en haut à gaucge et en bas à droite
        bloc2 = Bloc(b2_taille,True, couleur_b2)
        bloc3 = Bloc(b3_taille,True, couleur_b3)
        bloc4 = Bloc(b4_taille,True, couleur_b4)
        liste = [bloc1,bloc2,bloc3,bloc4]
        for element in liste:
            decouper_image(im, ordre-1, (element.get_taille()[0]),(element.get_taille()[1]), res)
        if distance_entre_deux_images(couleur_im, couleur_b1) and distance_entre_deux_images(couleur_im, couleur_b2) and distance_entre_deux_images(couleur_im, couleur_b3) and distance_entre_deux_images(couleur_im, couleur_b4):
            cl_moy = couleurs_moyenne([couleur_b1,couleur_b2,couleur_b3,couleur_b4])
            res.append(Bloc(basdroit, True, cl_moy))
        else:
            res.append(Bloc(basdroit, False))
    else:
        couleur_im = couleurs_moyenne(liste_couleur(im, (0,0), basdroit))
        res.append(Bloc(basdroit, True, couleur_im))
    return res

def mise_en_csv(liste):
    """
    Insere une liste dans un dossier csv
    §§§ mise_en_csv([('Angy','Wallot',19),('Facundo','Garcia',22)])
    """
    with open("blocs.csv", "w", newline="") as blocs:
        writer = csv.writer(blocs)
        for element in liste:
            writer.writerow(element)
            
if __name__ == "__main__":
    import sys
    if len(sys.argv) != 4:
        print("usage: classe.py chemin_image ordre hg bd liste_vide")
    else:
        chemin_image, ordre, hg, bd, liste_vide = sys.argv[1:]
        im = Image.open(chemin_image)
        im_rgb = im.convert('RGB')
        res = decouper_image(im, ordre, hg, bd, liste_vide)
        
        


    


        
