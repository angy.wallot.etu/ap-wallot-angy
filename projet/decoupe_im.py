from PIL import Image, ImageDraw        
        
def couleurs_moyenne(liste:list[tuple])->tuple:
    """
    FOnction qui calcule la moyenne dans une liste

    Précondition : Chaque tuple doit contenir 3 valeurs
    Exemple(s) :
    $$$ couleurs_moyenne([(1,2,3),(4,5,6),(7,8,9)])
    (4, 5, 6)
    $$$ couleurs_moyenne([(255, 0, 0), (0, 255, 0),(0, 0, 255)])
    (85, 85, 85)

    """
    r = 0
    g = 0
    b = 0
    for element in liste:
        r += element[0]
        g += element[1]
        b += element[2]
    return ((r//len(liste)), (g//len(liste)) ,(b//len(liste)))
    
def liste_couleur(image, coin_haut_gauche:tuple,coin_bas_droit:tuple)->list:
    """
    Fonction qui prend en parametre des coordonnées des pixels
    situés en haut à gauche du bloc et en bas à droite du bloc.
    Retourne une liste de couleur.

    Précondition : Chaque tuple doit contenir 2 valeurs
                   l'image doit etre au format rgb
    Exemple(s) :
#     $$$ im = Image.open('images/calbuth.png')
#     $$$ im_rgb = im.convert('RGB')
#     $$$ liste_couleur(im_rgb, (0,0),(10,10))
#     [(236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111)]

    """
    x_min, y_min = coin_haut_gauche
    x_max, y_max = coin_bas_droit
    return [image.getpixel((x,y))
            for x in range(x_min, x_max)
            for y in range(y_min, y_max)]

def distance_entre_deux_images(couleur1:tuple, couleur2:tuple)->bool:
    """
    Renvoie True si deux images ont des couleurs proches

    Précondition : couleur1 et couleur deux sont des tuples contenant 3 valeurs
    Exemple(s) :
    $$$ distance_entre_deux_images((252, 300, 80), (252, 260, 80))
    True
    $$$ distance_entre_deux_images((232, 240, 80), (252, 260, 50))
    True
    $$$ distance_entre_deux_images((232, 240, 80), (352, 260, 50))
    False
    $$$ distance_entre_deux_images((352, 240, 80), (232, 260, 50))
    False
    $$$ distance_entre_deux_images((232, 240, 80), (0, 0, 0))
    False

    """
    res = True
    for i in range(len(couleur1)):
        if couleur1[i] >= couleur2[i]:
            if (couleur1[i]-couleur2[i]) > 100:
                res = False
        if couleur1[i] < couleur2[i]:
            if (couleur2[i]-couleur1[i]) > 100:
                res = False
    return res

        
        
    
