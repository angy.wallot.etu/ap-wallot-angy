#17/01/24
#Angy WALLOT
#Anagrammes

from lexique import LEXIQUE

# METHODE SPLIT
# 1:
# >>> s.split(' ')
# ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
# 
# >>> s.split('é')
# ['la m', 'thode split est parfois bien utile']
# 
# >>> s.split()
# ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
# 
# >>> s.split('')
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# ValueError: empty separator
# 
# >>> s.split('split')
# ['la méthode ', ' est parfois bien utile']

# 2:
# La methode splits separe chaque element, si la fonction contient
# un paramètre avec un caractère ou un element celui ci n'est pas dans le renvoie de la fonction.

# 3:
# Non la methode split ne modifie pas la chaine à laquelle elle s'applique.

# METHODE JOIN
# 1:
# >>> "".join(l)
# 'laméthodesplitestparfoisbienutile'
# 
# >>> " ".join(l)
# 'la méthode split est parfois bien utile'
# 
# >>> ";".join(l)
# 'la;méthode;split;est;parfois;bien;utile'
# 
# >>> " tralala ".join(l)
# 'la tralala méthode tralala split tralala est tralala parfois tralala bien tralala utile'
# 
# >>> print ("\n".join(l))
# la
# méthode
# split
# est
# parfois
# bien
# utile
# 
# >>> "".join(s)
# 'la méthode split est parfois bien utile'
# 
# >>> "!".join(s)
# 'l!a! !m!é!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !p!a!r!f!o!i!s! !b!i!e!n! !u!t!i!l!e'
# 
# >>> "".join()
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# TypeError: str.join() takes exactly one argument (0 given)
# 
# >>> "".join([])
# ''
# 
# >>> "".join([1,2])
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# TypeError: sequence item 0: expected str instance, int found

# 2:
# La methode join ajoute l'element entre guillement entre chaque element de la chaine de caractere qui est passée en parametre.

# 3:
# Non la methode join ne modifie pas la chaine à laquelle elle s'applique.

#4:
def join(c : str, liste : list[str])->str:
    """
    envoie une chaîne de caractères construite en concaténant
    toutes les chaînes de liste en intercalant c

    Précondition : 
    Exemple(s) :
    $$$ join('.', ['raymond', 'calbuth', 'ronchin', 'fr'])
    'raymond.calbuth.ronchin.fr'
    """
    res = ''
    for i in range(len(liste)-1):
        res += liste[i] + c
    res += liste[len(liste)-1]
    return res

# METHODE SORT
# 1:
# >>> l = list('timoleon')
# >>> l
# ['t', 'i', 'm', 'o', 'l', 'e', 'o', 'n']
# >>> l.sort()
# >>> l
# ['e', 'i', 'l', 'm', 'n', 'o', 'o', 't']
# 
# >>> s = "Je n'ai jamais joué de flûte."
# >>> l = list(s)
# >>> l
# ['J', 'e', ' ', 'n', "'", 'a', 'i', ' ', 'j', 'a', 'm', 'a', 'i', 's', ' ', 'j', 'o', 'u', 'é', ' ', 'd', 'e', ' ', 'f', 'l', 'û', 't', 'e', '.']
# >>> l.sort()
# >>> l
# [' ', ' ', ' ', ' ', ' ', "'", '.', 'J', 'a', 'a', 'a', 'd', 'e', 'e', 'e', 'f', 'i', 'i', 'j', 'j', 'l', 'm', 'n', 'o', 's', 't', 'u', 'é', 'û']
# 
# La methode sort trie les caractères dans l'ordre alphabetique en plaçant les espaces devant, puis les caractères speciaux et les lettres.

#2:
# >>> l = ['a', 1]
# >>> l.sort()
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# TypeError: '<' not supported between instances of 'int' and 'str'
# Cela renvoie une erreur car la methode sort est conçue pour trier des elements de même type.

#1:
    
def sort(s: str)->str:
    """
    Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant.
    Précondition: aucune
    Exemples:
    $$$ sort('timoleon')
    'eilmnoot'
    """
    res = list(s)
    res.sort()
    res = "".join(res) 
    return res
    
def sont_anagrammes0(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes0('orange', 'organe')
    True
    $$$ sont_anagrammes0('orange','Organe')
    False
    $$$ sont_anagrammes0('orange','orangé')
    False
    """
    l1 = list(s1)
    l2 = list(s2)
    l1.sort()
    l2.sort()
    return l1 == l2
    
def sont_anagrammes1(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes1('orange', 'organe')
    True
    $$$ sont_anagrammes1('orange','Organe')
    False
    $$$ sont_anagrammes1('orange','orangé')
    False
    """
    res = {}
    res1 = {}
    for element in s1:
        if element in res:
            res[element] += 1
        else:
            res[element] = 1
    for el in s2:
        if el in res1:
            res1[el] += 1
        else:
            res1[el] = 1
    d1, d2 = list(res), list(res1)
    d1.sort()
    d2.sort()
    return d1 == d2

def sont_anagrammes2(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes2('orange', 'organe')
    True
    $$$ sont_anagrammes2('orange','Organe')
    False
    $$$ sont_anagrammes2('orange','orangé')
    False
    """
    for le in s1:
        if s1.count(le) != s2.count(le):
            return False
    return True
          
EQUIV_NON_ACCENTUE = {'à':'a','â':'a','ä':'a','é':'e','è':'e','ê':'e','ë':'e','ï':'i','î':'i','ô':'o','ö':'o','ù':'u','û':'u','ü':'u','ç':'c'}
        
def bas_casse_sans_accent(mot : str)->str:
    """
    renvoie une chaîne de caractères identiques à celle passée en paramètre sauf pour les lettres majuscules et les lettres accentuées qui sont converties en leur équivalent minuscule non accentué

    Précondition : 
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'

    """
    res = ''
    for lettre in mot:
        if lettre != lettre.lower():
            lettre = lettre.lower()
        if (lettre in EQUIV_NON_ACCENTUE) == True:
            lettre = EQUIV_NON_ACCENTUE[lettre]
            res += lettre
        else:
            res += lettre
    return res

def sont_anagrammes3(mot1:str, mot2:str)->bool:
    """
    prédicat qui ne différencie pas les lettres selon leur casse ou leur accentuation.

    Précondition : 
    Exemple(s) :
    $$$ sont_anagrammes3('Orangé', 'organE')
    True
    """
    return sont_anagrammes2(bas_casse_sans_accent(mot1), bas_casse_sans_accent(mot2))

def anagrammes5(mot: str) -> list[str]:
    """
    Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.

    Précondition: aucune

    Exemples:

    $$$ anagrammes5('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes5('info')
    ['foin']
    $$$ anagrammes5('Calbuth')
    []
    """
    res = []
    for i in range(len(LEXIQUE)):
        if sont_anagrammes3(LEXIQUE[i], mot) == True:
            res.append(LEXIQUE[i])
    return res
