#Angy Wallot
#gestion_promo_etudiants
#07/02/24

from date import Date
from etudiant import Etudiant

def pour_tous(seq_bool: bool) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous((True, True, True))
    True
    $$$ pour_tous((True, False, True))
    False
    """
    for el in seq_bool:
        if el == False:
            return el
    return True

def il_existe(seq_bool: bool) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe((False, True, False))
    True
    $$$ il_existe((False, False))
    False
    """
    for el in seq_bool:
        if el == True:
            return el
    return False

def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

L_ETUDIANTS = charge_fichier_etudiants("etudiants.csv")
COURTE_LISTE = L_ETUDIANTS[:10]

def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    res = isinstance(x, list)
    if res == True:
        for el in x:
            res = isinstance(el, Etudiant)
    return res 

NBRE_ETUDIANTS = len(L_ETUDIANTS)
NIP ="42312037"

def s(n):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ s(49423558)
    'Marcel Rocher'
    $$$ s(49452026)
    'Laetitia Sanchez'
    $$$ s(NIP)
    'non'
    """
    
    for el in L_ETUDIANTS:
        if el.nip == str(n):
            return el.prenom + " " +el.nom 
    return "non"

def li(l):
    res = []
    for i in range(len(l)):
        s = f"{l[i].naissance.jour}-{l[i].naissance.mois}-{l[i].naissance.annee}"
        if s < 2-2-2003:
            res.append(l[i].prenom)
    return res